package es.juegosmarcianos.mijuego;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Juego extends Game {
    SpriteBatch spriteBatch;
    BitmapFont bitmapFont;
    Music musicLLuvia;
    int record;

    public void create() {
        spriteBatch = new SpriteBatch();
        bitmapFont = new BitmapFont(false);
        bitmapFont.setColor(Color.CYAN);
        musicLLuvia = Gdx.audio.newMusic(Gdx.files.internal("lluvia.mp3"));
        musicLLuvia.setLooping(true);
        musicLLuvia.play();
        record = 0;
        setScreen(new PantallaMenu(this));
    }

    public void render() {
        super.render();
    }

    public void dispose() {
        spriteBatch.dispose();
        bitmapFont.dispose();
        musicLLuvia.dispose();
    }
}
