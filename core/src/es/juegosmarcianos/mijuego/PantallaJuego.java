package es.juegosmarcianos.mijuego;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Iterator;

/**
 * Created by marco on 8/10/16.
 */

public class PantallaJuego implements Screen {
    private Juego game;

    private Texture textureGota;
    private Texture textureCubo;
    private Texture textureCalabozo;
    private Texture textureAgua;
    private Texture textureMuerte;

    private Sound soundGota;
    private Sound soundMuerte;

    private OrthographicCamera camera;
    private Rectangle rectangleCubo;
    private Array<Rectangle> arrayGotas;
    private long tiempoGota, tiempoAhogado, tiempoAchique;
    private int puntos;
    private float nivelAgua;
    private boolean ahogado;

    private float anchoEscena, altoEscena;
    private float anchoCubo, anchoGota;
    private float velocidadCubo;


    public PantallaJuego(Juego juego) {
        this.game = juego;
        arrayGotas = new Array<Rectangle>();
        rectangleCubo = new Rectangle();

        textureGota = new Texture(Gdx.files.internal("gota.png"));
        textureCubo = new Texture(Gdx.files.internal("cubo.png"));
        textureCalabozo = new Texture(Gdx.files.internal("calabozo.jpg"));
        textureAgua = new Texture(Gdx.files.internal("agua.png"));
        textureMuerte = new Texture(Gdx.files.internal("muerte.png"));

        soundGota = Gdx.audio.newSound(Gdx.files.internal("gota.ogg"));
        soundMuerte = Gdx.audio.newSound(Gdx.files.internal("grito.ogg"));
    }

    @Override
    public void show() {
        anchoEscena = 640;
        altoEscena = 360;
        nivelAgua = -altoEscena;

        anchoCubo = 64;
        anchoGota = 32;

        velocidadCubo = 350;

        tiempoAchique = TimeUtils.millis();
        tiempoAhogado = 0;


        rectangleCubo.width = anchoCubo;
        rectangleCubo.height = anchoCubo;

        rectangleCubo.x = anchoEscena / 2 - anchoCubo / 2;
        rectangleCubo.y = 24;

        ahogado = false;


        camera = new OrthographicCamera();
        camera.setToOrtho(false, anchoEscena, altoEscena);

        spawnRaindrop();
    }

    private void spawnRaindrop() {
        Rectangle rectanguloGota = new Rectangle();
        rectanguloGota.x = MathUtils.random(0, anchoEscena - anchoGota);
        rectanguloGota.y = altoEscena;
        rectanguloGota.width = anchoGota;
        rectanguloGota.height = anchoGota;
        arrayGotas.add(rectanguloGota);
        tiempoGota = TimeUtils.millis();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.spriteBatch.setProjectionMatrix(camera.combined);
        game.spriteBatch.begin();
        game.spriteBatch.draw(textureCalabozo, 0, 0, anchoEscena, altoEscena);
        game.spriteBatch.draw(textureAgua, 0, nivelAgua, anchoEscena, altoEscena);


        for (Rectangle raindrop : arrayGotas) {
            game.spriteBatch.draw(textureGota, raindrop.x, raindrop.y);
        }
        game.spriteBatch.draw(textureCubo, rectangleCubo.x, rectangleCubo.y);
        if (ahogado) {
            game.spriteBatch.draw(textureMuerte, 0, 0, anchoEscena, altoEscena);
        }
        game.bitmapFont.draw(game.spriteBatch, "Gotas recogidas: " + puntos, 60, altoEscena - 20);
        game.bitmapFont.draw(game.spriteBatch, "Record: " + game.record, anchoEscena - 120, altoEscena - 20);
        game.spriteBatch.end();

        if (!ahogado) {
            //controles
            if (Gdx.input.isTouched()) {
                Vector3 touchPos = new Vector3();
                touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
                camera.unproject(touchPos);
                // rectangleCubo.x = touchPos.x - 64 / 2;
                if (touchPos.x > rectangleCubo.x + anchoCubo) {
                    rectangleCubo.x += velocidadCubo * delta;
                } else if (touchPos.x < rectangleCubo.x) {
                    rectangleCubo.x -= velocidadCubo * delta;
                }
            }
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
                rectangleCubo.x -= velocidadCubo * delta;
            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))
                rectangleCubo.x += velocidadCubo * delta;

            // controlamos que el cubo no se salga de la pantalla
            if (rectangleCubo.x < 0)
                rectangleCubo.x = 0;
            if (rectangleCubo.x > anchoEscena - anchoCubo)
                rectangleCubo.x = anchoEscena - anchoCubo;

            // creamos nuevas gotas
            if (TimeUtils.millis() - tiempoGota > 700) {
                spawnRaindrop();
            }

            // damos un respiro
            if (TimeUtils.millis() - tiempoAchique > 10000 && nivelAgua > -altoEscena) {
                nivelAgua -= 36;
                tiempoAchique = TimeUtils.millis();
                Gdx.app.log("Game", "agua achicada");
            }

            // movemos todas las gotas de la lista
            Iterator<Rectangle> iter = arrayGotas.iterator();
            while (iter.hasNext()) {
                Rectangle raindrop = iter.next();
                raindrop.y -= 180 * delta;
                if (raindrop.y + 64 < 0) {
                    iter.remove();
                    nivelAgua += 36;
                    tiempoAchique = TimeUtils.millis();
                }
                if (raindrop.overlaps(rectangleCubo)) {
                    puntos++;
                    soundGota.play();
                    iter.remove();
                }
            }


            //controlamos el final de la partida
            if (nivelAgua > -36) {
                ahogado = true;
                tiempoAhogado = TimeUtils.millis();
                soundMuerte.play();
                Gdx.app.log("Game", "finish");
            }
        }

        //si hemos terminado, esperamos, y volvemos a la pantalla principal
        if (ahogado) {
            if (TimeUtils.millis() - tiempoAhogado > 5000) {
                if (game.record < puntos) {
                    game.record = puntos;
                }

                game.setScreen(new PantallaMenu(game));
                dispose();
                Gdx.app.log("Game", "return");
            }
        }
    }

    @Override
    public void resize(int width, int height) {
    }


    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        textureGota.dispose();
        textureCubo.dispose();
        textureCalabozo.dispose();
        textureAgua.dispose();
        textureMuerte.dispose();
        soundGota.dispose();
        soundMuerte.dispose();

    }
}
