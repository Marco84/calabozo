package es.juegosmarcianos.mijuego;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by marco on 8/10/16.
 */

public class PantallaMenu implements Screen {

    private Juego game;
    private OrthographicCamera camera;
    private Texture fondo;


    public PantallaMenu(Juego juego) {
        game = juego;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 640, 360);
        fondo = new Texture(Gdx.files.internal("calabozo.jpg"));
    }

    @Override
    public void show() {

    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.spriteBatch.setProjectionMatrix(camera.combined);

        game.spriteBatch.begin();
        game.spriteBatch.draw(fondo, 0, 0, 640, 360);
        game.bitmapFont.draw(game.spriteBatch, "Record: " + game.record, 500, 340);
        game.bitmapFont.draw(game.spriteBatch, "¡¡Se te inunda el calabozo!! ", 80, 125);
        game.bitmapFont.draw(game.spriteBatch, "Utiliza el cubo para achicar las goteras y evitar ahogarte", 80, 100);
        game.bitmapFont.draw(game.spriteBatch, "Toca o click para empezar", 240, 30);
        game.spriteBatch.end();

        if (Gdx.input.isTouched()) {
            game.setScreen(new PantallaJuego(game));
            dispose();
        }
    }

    @Override
    public void resize(int width, int height) {
    }


    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        fondo.dispose();
    }
}
