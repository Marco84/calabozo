package es.juegosmarcianos.mijuego.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import es.juegosmarcianos.mijuego.Juego;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(640, 360);
        }

        @Override
        public ApplicationListener createApplicationListener () {
                return new Juego();
        }
}